FROM node:18
WORKDIR /usr/node-hello-world
COPY ./node-hello-world /usr/node-hello-world
RUN npm install 
RUN ls /bin
ENTRYPOINT npm start 
